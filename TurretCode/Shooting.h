//Header file that goes along with the source file for Shooting Class

#ifndef Shooting_h
#define Shooting_h

#include <Arduino.h>
#include <Servo.h>



class Shooting{
public:
  void initialiseRotation();
	void shootBullets(int i);  
     
};
  
  

#endif
