#include "Shooting.h"

Servo servoShooting;

int rot;
boolean forward = true;

int bullets = 10;
int motorpin = 11;

void Shooting::initialiseRotation() {
  servoShooting.attach(7);
  servoShooting.write(92);
  pinMode (motorpin, OUTPUT);
}

//int getNCPos() {
//  return servoShootingNC.read();
//}

void rotateMotor(int numRot) {
//  //non-continuous rotation
//  int j = 1;
//  do {
//    if (forward) {
//      rot = getNCPos() + 90;
//      for (int i = getNCPos(); i <= rot; i++) {
//        servoShootingNC.write(i);
//        Serial.println(getNCPos());
//        delay(15);
//      }
//      forward = false;
//    } 
//    else {
//      rot = getNCPos() - 90;
//      for (int i = getNCPos(); i >= rot; i--) {
//        servoShootingNC.write(i);
//        Serial.println(getNCPos());
//        delay(15);
//      }
//      forward = true;
//    }
//    j++;
//  }
//  while(j <= numRot);
//
//  //continuous rotation
//  servoShooting.write(180);
//  delay(350 * numRot);
//  servoShooting.write(92);

  servoShooting.write(0);
  delay(400*numRot);
  servoShooting.write(92);
  delay(1000);
  servoShooting.write(180);
  delay(400*numRot);
  servoShooting.write(92);
  delay(1000);

}



void Shooting::shootBullets(int i) {
  if (bullets >= i) {
    Serial.println("YASSSSSSSSSSSSSSSSSSSSSS");
    analogWrite(motorpin, 255);
    delay(4000);
    rotateMotor(i); 
    //delay(2000);
    analogWrite(motorpin, 0);
    bullets = bullets - i;
  }
  else if (bullets == 0) {
    Serial.print("There are no bullets left to fire.");
  }
  else {
    Serial.print("There are only ");
    Serial.print(bullets);
    Serial.print(" to be fired but ");
    Serial.print(i);
    Serial.print(" bullets were requested. Please pick a number less than ");
    Serial.print(bullets);
    Serial.print(".");
  }
}
