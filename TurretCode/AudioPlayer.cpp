/*
  AudioPLayer.cpp - Library for playing sounds yo.
  Created by Charlotte A. Wilson && Baldeep Sanghera, January 2016.
*/

#include "WaveHC.h"
#include "WaveUtil.h"
#include "AudioPlayer.h"
#include "SdReader.h"

AudioPlayer::AudioPlayer()
{
  Serial.begin(9600);
  randomSeed(analogRead(0));

  if (!card.init()) error("card.init");

  // enable optimized read - some cards may timeout
  card.partialBlockRead(true);

  if (!vol.init(card)) error("vol.init");
  if (!root.openRoot(vol)) error("openRoot");
}

void AudioPlayer::error_P(const char *str) {
  PgmPrint("Error: ");
  SerialPrint_P(str);
  //while(1); if error won't play file but continues anyways - WILD!
}

void AudioPlayer::playSound(const char *str)
{
  int size = strlen(str);
  char name[size]; 
  FatReader folder;

  strcpy(name, str);
  //Serial.println(name);

  if(!folder.open(root, name)) error("open folder");
  Serial.println("opened folder");
  
  if(!file.open(folder, "10.WAV")) error("open file");
  Serial.println("opened file");

   // create wave and start play
    if (!wave.create(file))error("wave.create");
    wave.play();
    while (wave.isplaying) {
  // do nothing while its playing
  }
}

