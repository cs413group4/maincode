//Header file that goes along with the source file for Movement Class

#ifndef Movement_h
#define Movement_h

#include <Arduino.h>
#include <Servo.h>



class Movement{
public:
        void initialiseMovement();
	      void moveBody();
        void rotate(boolean b);
	      void resetPosition();  
        void searchForTarget();
        void mapArea();
        int getNumRotation();
};

#endif
