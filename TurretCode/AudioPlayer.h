/*
  AudioPLayer.h - Library for playing sounds yo.
  Created by Charlotte A. Wilson && Baldeep Sanghera, January 2016.
*/

#ifndef AudioPlayer_h
#define AudioPlayer_h

#include <Arduino.h>

#define error(msg) error_P(PSTR(msg))

class AudioPlayer
{
  public:
    AudioPlayer();
    void playSound(const char *str);
  private:
    SdReader card;    // This object holds the information for the card
    FatVolume vol;    // This holds the information for the partition on the card
    FatReader root;   // This holds the information for the volumes root directory
    FatReader file;   // This object represent the WAV file
    WaveHC wave;      // This is the only wave (audio) object, since we will only play one at a time
    dir_t dirBuf;     // buffer for directory reads
    void error_P(const char *str);

};

#endif

