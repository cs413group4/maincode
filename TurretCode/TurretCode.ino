
/* 
 * The PIR sensor's output pin goes to HIGH if motion is present.
 * However, even if motion is present it goes to LOW from time to time, 
 * which might give the impression no motion is present. 
 * This program deals with this issue by ignoring LOW-phases shorter than a given time, 
 * assuming continuous motion is present during these phases.
 *  
 */
#include <Arduino.h>

#include "AudioPlayer.h"
#include "WaveHC.h"
#include "WaveUtil.h"
#include "SdReader.h"

#include "Movement.h"
//0,1,7,8,12,
//pwm 6,9,11,
Movement mov;
AudioPlayer ap;

//the time we give the sensor to calibrate (10-60 secs according to the datasheet)
int calibrationTime = 5;        

//the time when the sensor outputs a low impulse
volatile long unsigned int timeDetected;         

//the amount of milliseconds the sensor has to be low 
//before we assume all motion has stopped
long unsigned pause = 2000;    

volatile boolean lockLow = true;
volatile boolean movementDetected = false;  

int const tiltPin = 2;
int const pirPin = 6;    
int const pingTrigPin =12;// 5;
int const pingEchoPin =8;// 4;
int const ledPin = 13;

/////////////////////////////
//SETUP
void setup(){
  Serial.begin(9600);
  pinMode(pirPin, INPUT);
  pinMode(ledPin, OUTPUT);
  pinMode(pingTrigPin,OUTPUT);
  pinMode(pingEchoPin,INPUT);
  pinMode(tiltPin, INPUT);
  ap.playSound("1");
  digitalWrite(pirPin, LOW);
  
  mov.initialiseMovement();
  mov.resetPosition();

  //SOUND: some starting up soundbytes
  Serial.println("Initialisation commencing");
  //give the sensor some time to calibrate
  Serial.print("Calibrating Sensor ");
    for(int i = 0; i < calibrationTime; i++){
      Serial.print(".");
      delay(1000);
      }
    Serial.println("SENSOR ACTIVE");
    delay(50);
    mov.mapArea();
    Serial.println("Initialisation complete");
    mov.resetPosition();
    //digitalPinToInterrupt(pirPin) & (tiltPin) doesn't work in older IDE's. Replace ints 3 & 2 with digitalPinToInterrupt
    //attachInterrupt(1, warmBodyDetected, HIGH);  //registering Pin 3 for PIR sensor to output high
    //attachInterrupt(0, playMovingSound, LOW);   //registering Pin 2 for tile sensor to output low

  }
  
void loop(){
      /*If system is meant to be doing something periodically doing stuff
       * well no warm body is present then out that here
       */
    //  mov.moveBody();
     while ( digitalRead(pirPin) == HIGH){
      //SOUND: PIR sensor has found a warm body and now proceds to search for target
    mov.rotate(false);
      mov.moveBody();
      if(lockLow) {
        timeDetected = millis();
         lockLow = false;            
         Serial.println("---");
         Serial.print("motion detected at ");
         Serial.print(millis()/1000);
         Serial.println(" sec"); 
              
         mov.rotate(true);
         mov.searchForTarget();
         movementDetected = true;
      }
     }
     
      if(digitalRead(pirPin) == LOW && movementDetected){    //has movement recently been detected
          if(millis() - timeDetected > pause){
             //makes sure this block of code is only executed again after 
             //a new motion sequence has been detected
             lockLow = true;                        
             Serial.print("motion ended at ");      //output
             Serial.print((millis() - pause)/1000);
             Serial.println(" sec");
             delay(50);
             mov.rotate(false);
             movementDetected=false;
             }
       }
//       if(lockLow && digitalRead(pirPin) == HIGH){  
//         //makes sure we wait for a transition to LOW before any further output is made:
//         timeDetected = millis();
//         lockLow = false;            
//         Serial.println("---");
//         Serial.print("motion detected at ");
//         Serial.print(millis()/1000);
//         Serial.println(" sec"); 
//              
//         mov.rotate(false);
//         mov.searchForTarget();
//         movementDetected = true;
//  }
  
  }

//void warmBodyDetected(){ 
//  if(lockLow){  
//         //makes sure we wait for a transition to LOW before any further output is made:
//         timeDetected = millis();
//         lockLow = false;            
//         Serial.println("---");
//         Serial.print("motion detected at ");
//         Serial.print(millis()/1000);
//         Serial.println(" sec"); 
//              
//         mov.rotate(false);
//         mov.searchForTarget();
//         movementDetected = true;
//  }
//}

void playMovingSound(){
      //Input adafruit sound playing code here pls
      Serial.println("Playing sounds such as ''Where are we going?'' and ''Ahhhh'' ");
}

