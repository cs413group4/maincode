#include "Movement.h" //include the declaration for this class

#include <Arduino.h>
#include <Servo.h> 
#include "Shooting.h"

Shooting shoot;
long mapping[7]; 

 Servo servoMovement; // create servo object to control a servo 
 int posMovement;    // variable to store the servo position 
 int pingTrigPin = 12;
 int pingEchoPin = 8;
 int numRot = 0;



 boolean rotation = true;
 boolean targetFound = false;

 
//const byte LED_PIN = 13; //use the LED @ Arduino pin 13

//<<constructor>> setup the LED, make pin 13 an OUTPUT
//Movement::~Movement(){
     // attaches the servo on pin 9 to the servo object 
	
	// }

//<<destructor>>
//Movement::~Movement(){/*nothing to destruct*/}

void Movement::initialiseMovement() {
  servoMovement.attach(9);
  shoot.initialiseRotation();
}

long getDistance()
{
  digitalWrite(pingTrigPin, LOW);
  delayMicroseconds(2);
  digitalWrite(pingTrigPin, HIGH);     //why these pin writings?
  delayMicroseconds(5);
  digitalWrite(pingTrigPin, LOW);
  long duration = pulseIn(pingEchoPin, HIGH);
  int microSeconds = duration/29/2;
  return microSeconds;
}



void Movement::moveBody(){

//   if(numRot == 3) {
//     numRot=0;
//      return;
//    }

  for(posMovement=servoMovement.read(); posMovement>=0; posMovement--) {
    if(!rotation) {
      break;
    }
   
    servoMovement.write(posMovement);
    delay(15);
  }

  
  for(posMovement=servoMovement.read(); posMovement<=180; posMovement++) {
    if(!rotation) {
      break;
    }
 
    servoMovement.write(posMovement);
    delay(15);
  }
//  numRot++;
  
  
//  //servoMovement.attach(9);
//  if(!stopMov) {
//    for(posMovement = currentPos; posMovement <= 180; posMovement += 1) // goes from 0 degrees to 180 degrees 
//  {                                  // in steps of 1 degree 
//    servoMovement.write(posMovement);              // tell servo to go to position in variable 'pos' 
//    delay(15);                       // waits 15ms for the servo to reach the position 
//  } 
//  for(posMovement = currentPos; posMovement>=0; posMovement-=1)     // goes from 180 degrees to 0 degrees 
//  {                                
//    servoMovement.write(posMovement);              // tell servo to go to position in variable 'pos' 
//    delay(15);                       // waits 15ms for the servo to reach the position 
//  } 
//  }
}

void Movement::rotate(boolean b) {
  rotation = b;
}


/* mapping[7]'s eight indexes maps to 0-180 degrees.
  Each increment of one in the array maps to an increment of 30
  in the range. I thought that provided sufficient accuracy. Or
  at least that was as accurate as the PING sensor could be.
*/

void Movement::mapArea(){
  Serial.print("Mapping Area");
  for(posMovement = 0; posMovement <= 180; posMovement += 1) 
  {                                    
  servoMovement.write(posMovement);              
  if((posMovement%30)==0)                     //Is the current position one which we take a measurement    //???????????????????????????????????????? if(!(posMovement%30))
  {   
    Serial.print(".");
    mapping[posMovement/30]=getDistance();  //Find the correct array index 
    Serial.print(mapping[posMovement/30]);
  }
  delay(15);                      
  } 
  Serial.println("");
}
  
void Movement::searchForTarget()
{ // The PIR sensor knows something is there, now the ping sensor needs to find it



//  int i = servoMovement.read()/30;
//  long j = getDistance();
//  if(j<mapping[i]) {
//    Serial.print("target acquired at ");
//    Serial.print(j);
//    Serial.print(" away at from an original distance of ");
//    Serial.print(i);
//    Serial.print(". Detected at ");
//    Serial.print(servoMovement.read());
//    Serial.print("degrees.");
//  }


  targetFound = false;
while ( numRot < 3) {
  
  for(posMovement = servoMovement.read(); posMovement <= 180; posMovement += 1) 
  {                                  
     servoMovement.write(posMovement);              
     if(((posMovement%30)==0)&&(getDistance()<mapping[posMovement/30])&&(!targetFound)) 
     //Is the current position one which we took a measurement and is the new measurement lower than the stored one
     {
       Serial.print("target acquired at "); 
       Serial.print(posMovement);
       Serial.println(" degrees.");
       Serial.println(getDistance());
       shoot.shootBullets(1);
       targetFound = true;
       break;
     }
    delay(15);                       // waits 15ms for the servo to reach the position 
  } 
  if (targetFound){
    numRot=0;
    break;
  }
  
  for(posMovement = servoMovement.read(); posMovement>=0; posMovement-=1)     // goes from 180 degrees to 0 degrees 
  {             
    if(targetFound) {
      break;                    
    }
     servoMovement.write(posMovement);              // tell servo to go to position in variable 'pos' 
     if((!(posMovement%30))&&(getDistance()<mapping[posMovement/30])&&(!targetFound))
    {
       Serial.print("target acquired at "); 
       Serial.print(posMovement);
       Serial.println(" degrees.");
       shoot.shootBullets(1);
       targetFound = true;
       break;
    }
   delay(15);                       // waits 15ms for the servo to reach the position 
  }

   if (targetFound){
    numRot=0;
    break;
  }

  numRot++;
}

numRot=0;

  if(!targetFound && digitalRead(6)==LOW) {
    Serial.println("target not acquired");
  }

  if(!targetFound && digitalRead(6)==HIGH){
    Serial.println("target still within range of PING sensor but is moving");
  }

  
}



int Movement::getNumRotation(){
  return numRot;
}

void Movement::resetPosition(){
  servoMovement.write(90);
}

