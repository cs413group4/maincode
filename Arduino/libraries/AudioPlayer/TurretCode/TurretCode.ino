
/*
   The PIR sensor's output pin goes to HIGH if motion is present.
   However, even if motion is present it goes to LOW from time to time,
   which might give the impression no motion is present.
   This program deals with this issue by ignoring LOW-phases shorter than a given time,
   assuming continuous motion is present during these phases.

*/
#include <Arduino.h>
//#include <AudioPlayer.h>

#include "Movement.h"
Movement mov;
//AudioPlayer ap;

//the time we give the sensor to calibrate (10-60 secs according to the datasheet)
//int calibrationTime = 5;

//the time when the sensor outputs a low impulse
//volatile long unsigned int timeDetected;

//the amount of milliseconds the sensor has to be low
//before we assume all motion has stopped
//long unsigned pause = 2000;

//volatile boolean lockLow = true;
//volatile boolean movementDetected = false;

/////////////////////////////
//SETUP
void setup() {
  int const pingEchoPin = 8;
  int const pirPin = 6;
  int const pingTrigPin = 12;
  Serial.begin(9600);
  //Serial.println("start");
  //ap.playSound("get_mad");
  pinMode(pirPin, INPUT);
//  pinMode(ledPin, OUTPUT);
  pinMode(pingTrigPin, OUTPUT);
  pinMode(pingEchoPin, INPUT);
//  pinMode(tiltPin, INPUT);
  digitalWrite(pirPin, LOW);

  mov.initialiseMovement();
 // mov.resetPosition();

  //SOUND: some starting up soundbytes
 // Serial.println(F("Initialisation commencing"));
  //give the sensor some time to calibrate
  Serial.print(F("Calibrating Sensor "));
  for (int i = 0; i < 5; i++) {
    Serial.print(F("."));
    delay(1000);
  }
  //Serial.println(F("SENSOR ACTIVE"));
  delay(50);
  mov.mapArea();
  Serial.println(F("Initialisation complete"));
  //mov.resetPosition();
  //digitalPinToInterrupt(pirPin) & (tiltPin) doesn't work in older IDE's. Replace ints 3 & 2 with digitalPinToInterrupt
  //attachInterrupt(1, warmBodyDetected, HIGH);  //registering Pin 3 for PIR sensor to output high
  //attachInterrupt(0, playMovingSound, LOW);   //registering Pin 2 for tile sensor to output low

}
void memSave(){
  Serial.println(F(" sec"));
      delay(50);
}

void loop() {
  /*If system is meant to be doing something periodically doing stuff
     well no warm body is present then out that here
  */
  volatile boolean lockLow = true;
  volatile boolean movementDetected = false;
  volatile long unsigned int timeDetected;
  //ap.playSound("get_mad");
  //  mov.moveBody();
  while ( digitalRead(6) == HIGH) {
    //SOUND: PIR sensor has found a warm body and now proceds to search for target
    
    if (lockLow) {
      timeDetected = millis();
      lockLow = false;
      Serial.println(F("---"));
      Serial.print(F("motion detected at "));
      Serial.print(millis() / 1000);
      memSave();
      //mov.rotate(true);
      mov.searchForTarget();
      movementDetected = true;
    }
  }

  if (digitalRead(6) == LOW && movementDetected) {  //has movement recently been detected
    if (millis() - timeDetected > 2000) {
      //makes sure this block of code is only executed again after
      //a new motion sequence has been detected
      lockLow = true;
      Serial.print(F("motion ended at "));      //output
      Serial.print((millis() - 2000) / 1000);
     memSave();
      //mov.rotate(false);
      movementDetected = false;
    }
  }


}




