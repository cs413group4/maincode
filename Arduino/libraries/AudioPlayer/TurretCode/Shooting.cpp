#include "Shooting.h"

#include "ServoTimer2.h"
ServoTimer2 servoShooting;
//Servo servoShooting;
//int rot;


void Shooting::initialiseRotation() {
  servoShooting.attach(7);
 // servoShooting.write(92);
  pinMode (11, OUTPUT);
  //servoShooting.write(0);
   servoShooting.write(1490);
}

//int getNCPos() {
//  return servoShootingNC.read();
//}

void rotateMotor(int numRot) {
//  //non-continuous rotation
//  int j = 1;
//  do {
//    if (forward) {
//      rot = getNCPos() + 90;
//      for (int i = getNCPos(); i <= rot; i++) {
//        servoShootingNC.write(i);
//        Serial.println(getNCPos());
//        delay(15);
//      }
//      forward = false;
//    } 
//    else {
//      rot = getNCPos() - 90;
//      for (int i = getNCPos(); i >= rot; i--) {
//        servoShootingNC.write(i);
//        Serial.println(getNCPos());
//        delay(15);
//      }
//      forward = true;
//    }
//    j++;
//  }
//  while(j <= numRot);
//
//  //continuous rotation
//  servoShooting.write(180);
//  delay(350 * numRot);
//  servoShooting.write(92);

 //servoShooting.write(0);
  servoShooting.write(1300);
  delay(400*numRot);
servoShooting.write(1490);
//  servoShooting.write(92);
  delay(1000);
  // servoShooting.write(180);
  servoShooting.write(1700);
  delay(400*numRot);
//  servoShooting.write(92);
   servoShooting.write(1490);
  delay(1000);
servoShooting.write(1700);
  delay(400*numRot);
  servoShooting.write(1490);
  delay(1000);
  servoShooting.write(1300);
  delay(400*numRot);
  servoShooting.write(1490);
  delay(1000);
}



void Shooting::shootBullets(int i) {
  int bullets = 10;
  int motorpin = 11;
  if (bullets >= i) {
    analogWrite(motorpin, 255);
    delay(2000);
    rotateMotor(i); 
    //delay(2000);
    analogWrite(motorpin, 0);
    bullets = bullets - i;
  }
  else if (bullets == 0) {
    Serial.print(F("There are no bullets left to fire."));
  }
  else {
    Serial.print(F("There are only "));
    Serial.print(bullets);
    Serial.print(F(" to be fired but "));
    Serial.print(i);
    Serial.print(F(" bullets were requested. Please pick a number less than "));
    Serial.print(bullets);
    Serial.print(F("."));
  }
}
