#include "Movement.h" //include the declaration for this class

#include <Arduino.h>
//#include "ServoTimer2.h"
#include "Shooting.h"
//#include <Servo.h>

//Servo servoMovement;
Shooting shoot;
long mapping[6];

ServoTimer2 servoMovement; // create servo object to control a servo
//int posMovement;    // variable to store the servo position
//boolean rotation = true;
boolean targetFound = false;

void Movement::initialiseMovement() {
  servoMovement.attach(9);
// Serial.println(servoMovement.attached());
  shoot.initialiseRotation();
}

long getDistance()
{
  int pingTrigPin = 12;
  int pingEchoPin = 8;
  digitalWrite(pingTrigPin, LOW);
  delayMicroseconds(2);
  digitalWrite(pingTrigPin, HIGH);     //why these pin writings?
  delayMicroseconds(5);
  digitalWrite(pingTrigPin, LOW);
 // long duration = pulseIn(pingEchoPin, HIGH);
 // int microSeconds = (pulseIn(pingEchoPin, HIGH)) / 29 / 2;
  return (pulseIn(pingEchoPin, HIGH)) / 29 / 2;
}


/* mapping[7]'s eight indexes maps to 0-180 degrees.
  Each increment of one in the array maps to an increment of 30
  in the range. I thought that provided sufficient accuracy. Or
  at least that was as accurate as the PING sensor could be.
*/

void Movement::mapArea() {

    // this gets good distance values from i = 45 down to i = 12 then bad distance
  int count = 0;
  int i;
  int num1;

  servoMovement.write(750);
  //for(i=0;i<30;i++){
    for(i=0;i<30;i++){
    num1=(servoMovement.read()+50);
    delay(5);
    servoMovement.write(num1);
    
    if ((i % 5) == 0)                //Is the current position one which we take a measurement    //?????????????????????????????????????? if(!(posMovement%30))
    
    {
      Serial.print(F("."));
     mapping[i / 5] = getDistance(); //Find the correct array index
     mapping[i] = getDistance();
      Serial.print(mapping[i / 5]);
      //Serial.print(mapping[i]);
    }
    delay(50);
  }
//  
//  for ( i = 1; i != 60; i++){     // servo starts in the middle so for the first loop it hangs from i = 30  until i = 60
//    if ( i <= 15 ){                  // then it sweeps back and forth ~180 deg correctly
//      num1 = (servoMovement.read() - 50);
//      Serial.println(servoMovement.read()-50);
//      delay(5);      
//      servoMovement.write(num1);
//      delay(50);
//      count = count + 1;
//      Serial.print(i);
//      Serial.println("  i < 15  ");
//    }
//    else{
//      num1 = (servoMovement.read() + 50);
//      Serial.println(servoMovement.read()+50);
//      delay(5);
//      servoMovement.write(num1);
//      delay(50); 
//      count = count + 1;
//      Serial.print(i);
//      Serial.println("  i > 15  ");
//    }
//  }

  
//  for (posMovement = 0; posMovement <= 180; posMovement += 1)
//  {
//
//    posMovement += 2250;
//    servoMovement.write(posMovement);
//    posMovement -= 2250; 
//   // Serial.println(servoMovement.read());
//    if ((posMovement % 30) == 0)                //Is the current position one which we take a measurement    //???????????????????????????????????????? if(!(posMovement%30))
//    {
//      Serial.print(F("."));
//      mapping[posMovement / 30] = getDistance(); //Find the correct array index
//      Serial.print(mapping[posMovement / 30]);
//    }
//  }  delay(15);
//  
}

void Movement::searchForTarget()
{ // The PIR sensor knows something is there, now the ping sensor needs to find it

int numRot = 0;
  int posMovement;
  targetFound = false;
  servoMovement.write(750);
  while ( numRot < 3) {

   // servoMovement.write(750);
  for(int posMovement=0;posMovement<30;posMovement++){
 
    int num1=(servoMovement.read()+50);
    delay(5);
    servoMovement.write(num1);
    //      //  /5
    if (((posMovement % 5) == 0)  && ((getDistance()+4) < mapping[posMovement /5]) && (!targetFound))
        //Is the current position one which we took a measurement and is the new measurement lower than the stored one
      {
        Serial.print(F("target acquired at "));
        Serial.print(posMovement);
        Serial.println(F(" degrees."));
        Serial.println(getDistance());
        shoot.shootBullets(1);
        targetFound = true;
        break;
      }
    delay(50);
  }
  if (targetFound) {
      numRot = 0;
      break;
    }

    for(int posMovement=0;posMovement<30;posMovement++){
    int num1=(servoMovement.read()-50);
    delay(5);
    servoMovement.write(num1);
    if (((posMovement % 5) == 0) && ((getDistance()+4) < mapping[posMovement/5]) && (!targetFound))
        //Is the current position one which we took a measurement and is the new measurement lower than the stored one
      {
        Serial.print(F("target acquired at "));
        Serial.print(posMovement);
        Serial.println(F(" degrees."));
        Serial.println(getDistance());
        shoot.shootBullets(1);
        targetFound = true;
        break;
      }
    delay(50);
  }
  if (targetFound) {
      numRot = 0;
      break;
    }
    


    numRot++;
  }

  numRot = 0;

  if (!targetFound && digitalRead(6) == LOW) {
    Serial.println(F("target not acquired"));
  }

  if (!targetFound && digitalRead(6) == HIGH) {
    Serial.println(F("target still within range of PING sensor but is moving"));
  }


}



int Movement::getNumRotation() {
  int numRot = 0;
  return numRot;
}

void Movement::resetPosition() {
  servoMovement.write(1);
}

