//int vcc = 2; //attach pin 2 to vcc
int trig = 3; // attach pin 3 to Trig
int echo = 4; //attach pin 4 to Echo
//int gnd = 5; //attach pin 5 to GND
int ledPin = 13;
int pos=0;
long mappings[8];

void setup() {

//pinMode (vcc,OUTPUT);
//pinMode (gnd,OUTPUT);
// initialize serial communication:
Serial.begin(9600);
Serial.print("print");
pinMode(trig,OUTPUT);
pinMode(echo,INPUT);
servoSweep();
printArray();
}

long getDistance()
{
  digitalWrite(trig, LOW);
  delayMicroseconds(2);
  digitalWrite(trig, HIGH);
  delayMicroseconds(5);
  digitalWrite(trig, LOW);

  long duration = microsecondsToCentimeters(pulseIn(echo, HIGH));
  Serial.print(duration);
  return duration;
    
}

void loop(){
 long duration, cm; 
 
 
  //duration = pulseIn(echo, HIGH);
  //printArray();
  //Serial.print("");
  //Serial.print("cm");
  //Serial.println();
  //open console   tools-> serial monitor
}

int microsecondsToCentimeters(long microseconds)
{
  //340m/s = 29cm/ms
  //and half the distance 
 return  microseconds/29/2;
  
}

void servoSweep()
{
  
  for(pos = 0; pos <= 180; pos += 1) // goes from 0 degrees to 180 degrees 
  {                                  // in steps of 1 degree 
    //myservo.write(pos);              // tell servo to go to position in variable 'pos' 
    if(!(pos%30))
    {
      mappings[pos/30]=getDistance();
    }
    delay(15);                       // waits 15ms for the servo to reach the position 
  } 
  for(pos = 180; pos>=0; pos-=1)     // goes from 180 degrees to 0 degrees 
  {                                
    //myservo.write(pos);              // tell servo to go to position in variable 'pos' 
    if(!(pos%30))
    {
      mappings[pos/30]=getDistance();
    }
    delay(15);                       // waits 15ms for the servo to reach the position 
  }
}

void printArray()
{
  for(int i=0;i<8;i++)
  {
    Serial.print("m ");
    Serial.print(mappings[i]);
   Serial.println();
  }
}

//proof concept, need revision/refinement
