//int vcc = 2; //attach pin 2 to vcc
int trig = 3; // attach pin 3 to Trig
int echo = 4; //attach pin 4 to Echo
//int gnd = 5; //attach pin 5 to GND
int ledPin = 13;
void setup() {

//pinMode (vcc,OUTPUT);
//pinMode (gnd,OUTPUT);
// initialize serial communication:
Serial.begin(9600);
Serial.print("print");
}

void loop(){
 long duration, cm; 
  
  pinMode(trig,OUTPUT);
  digitalWrite(trig, LOW);
  delayMicroseconds(2);
  digitalWrite(trig, HIGH);
  delayMicroseconds(5);
  digitalWrite(trig, LOW);
  
  pinMode(echo,INPUT);
  duration = pulseIn(echo, HIGH);
  
  cm = microsecondsToInches(duration);
  
  Serial.print(cm);
  Serial.print("cm");
  Serial.println();
  //open console   tools-> serial monitor
}

long microsecondsToInches(long microseconds)
{
  //340m/s = 29cm/ms
  //and half the distance 
 return  microseconds/29/2;
  
}

